.PHONY: build setup fmt install lint test setup-ci clean watch test-race release

BINARY_NAME := sidecar
ARCH ?= darwin

MAIN_PKG := bitbucket.org/atlassian/microservice-sidecar-go/cmd/sidecar
GOVERSION := 1.7
REPO_VERSION := 0.0.2

all: clean setup build lint

setup: setup-ci
	go get -u github.com/githubnemo/CompileDaemon
	go get -u github.com/jstemmer/go-junit-report
	go get -u golang.org/x/tools/cmd/goimports

setup-ci:
	go get -u github.com/Masterminds/glide
	go get -u github.com/alecthomas/gometalinter
	gometalinter --install
	glide install --strip-vendor

build: fmt
	env GOOS=darwin GOARCH=amd64 go build -o build/bin/darwin.amd64/$(BINARY_NAME) $(GOBUILD_VERSION_ARGS) $(MAIN_PKG)
	chmod +x build/bin/darwin.amd64/$(BINARY_NAME)
	gzip build/bin/darwin.amd64/$(BINARY_NAME) -c > "build/bin/darwin.amd64/$(BINARY_NAME)-${REPO_VERSION}.darwin-amd64.gz"

	env GOOS=linux GOARCH=amd64 go build -o build/bin/linux.amd64/$(BINARY_NAME) $(GOBUILD_VERSION_ARGS) $(MAIN_PKG)
	chmod +x build/bin/linux.amd64/$(BINARY_NAME)
	gzip build/bin/linux.amd64/$(BINARY_NAME) -c > "build/bin/linux.amd64/$(BINARY_NAME)-${REPO_VERSION}.linux-amd64.gz"
fmt:
	gofmt -w=true -s $$(find . -type f -name '*.go' -not -path "./vendor/*")
	goimports -w=true -d $$(find . -type f -name '*.go' -not -path "./vendor/*")

lint:
	go install ./cmd/sidecar
	gometalinter --fast --vendor ./...

test:
	go test $$(glide nv)

test-race:
	go test -race $$(glide nv)

clean:
	rm -rf build/bin/*

release:
	./upload.sh "build/bin/darwin.amd64/$(BINARY_NAME)-${REPO_VERSION}.darwin-amd64.gz"
	./upload.sh "build/bin/linux.amd64/$(BINARY_NAME)-${REPO_VERSION}.linux-amd64.gz"
