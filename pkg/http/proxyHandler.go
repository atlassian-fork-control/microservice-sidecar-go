package http

import (
	"net/http"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/vulcand/oxy/forward"
	"github.com/vulcand/oxy/testutils"
)

// NewProxy returns a new proxy http handler
func NewProxy(viper *viper.Viper) (http.Handler, error) {
	log.SetFormatter(&log.JSONFormatter{})
	urlString := viper.GetString("proxyTargetUrl")
	fwd, err := forward.New(forward.Rewriter(reqRewriter{url: urlString}))
	if err != nil {
		return nil, err
	}
	return fwd, nil
}

type reqRewriter struct {
	url string
}

func (r reqRewriter) Rewrite(req *http.Request) {
	req.URL = testutils.ParseURI(r.url)
}
