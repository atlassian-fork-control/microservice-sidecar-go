package swagger

import (
	"io/ioutil"
	"net/http"
	"sync"

	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/runtime/middleware/untyped"
	"github.com/spf13/viper"
)

// NewMiddleware returns a new swagger validation middleware
func NewMiddleware(proxyHandler http.Handler, viper *viper.Viper) (http.Handler, error) {
	swaggerFile := viper.GetString("swaggerFilePath")
	spec, err := loadSwaggerSpec(swaggerFile)
	if err != nil {
		return nil, err
	}
	routableAPI := &routableUntypedAPI{
		spec:            spec,
		api:             untyped.NewAPI(spec),
		hlock:           new(sync.Mutex),
		defaultProduces: runtime.JSONMime,
		defaultConsumes: runtime.JSONMime,
	}

	context := middleware.NewRoutableContext(spec, routableAPI, nil)
	routableAPI.setupHandlers(proxyHandler, context)
	return context.APIHandler(nil), nil
}

func loadSwaggerSpec(swaggerFile string) (*loads.Document, error) {
	rawSwagger, err := ioutil.ReadFile(swaggerFile)
	if err != nil {
		return nil, err
	}

	spec, er := loads.Analyzed(rawSwagger, "")
	if er != nil {
		return nil, er
	}
	return spec, nil
}
